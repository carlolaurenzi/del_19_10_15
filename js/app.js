'use strict;'


jsloaded();

document.body.onload = defineEventOnElement;

function jsloaded() {
    var blockloaded = document.querySelector("#js-loaded");
    blockloaded.style.backgroundColor = 'GREEN';
    blockloaded.innerHTML = '<h1>Hello le Js files </h1>';
    return "OK!!!!"
}

function makeNote(note) {
    var divOfNote = document.createElement('div');
    //divOfNote.class = "note shad";
    divOfNote.id = "note-" + note.id;
    divOfNote.innerHTML =
        '<div id="campo-testo1" class="bord note">\
                    <div style="text-align: right; margin-bottom: -20px;" class="close-container">\
                        <img class="fit-picture" src="../images/closewindow.png" style="width: 20px;height:20px;">\
                    </div>\
                    <div class="visual-note-name">' + note.name +
        '</div><div style="text-align: center;">\
                        <span class="visual-note-date">' + note.date.toDateString() + '</span>&nbsp;\
                        <span class="visual-note-heure">00:00</span>\
                    </div>\
                    <div class="visual-note-desc">' + note.time.toTimeString() + '</div>' +
        divOfNote.querySelector('.close-container img').addEventListener('click', deleteNote);
    document.querySelector('#liste-notes').append(divOfNote);
}

function deleteNote(sender) {
    sender.target.parentElement.parentElement.remove();
}

/** 
 * function Event(arguments) {
 *     console.log(arguments);
 *    console.log(arguments[0]); }
 */

function defineEventOnElement(evt) {
    var noteUn = document.querySelector('#campo-testo1 div.close-container img');
    noteUn.addEventListener('click', deleteNote);

    var buttons = document.forms[0].querySelectorAll('button');
    buttons[0].addEventListener('click', resetNoteForm);
    buttons[1].addEventListener('click', submitForm);
}

function submitForm(evt) {
    document.querySelector('#edition-note').style.display = 'none';
    var form = document.forms["form-note"]
    form.submit();
    form.reset();
    console.log('oki');
}

function resetNoteForm() {
    document.forms[0].reset();
}

function nxhr() {

    var xhr = new XMLHttpRequest();
    xhr.open('GET', 'http://localhost:7500/notes', true);
    xhr.onreadystatechange = function(response) {
        if (response.target.readyState == 4 && response.target.status >= 200 && response.target.status < 300) {
            var obj = JSON.parse(response.target.response)

            console.log(response.target.status, response.target.response, obj);


        }
    }
    xhr.send();
}
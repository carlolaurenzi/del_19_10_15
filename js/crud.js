function RestCRUD(url, ressourceName) {
    var _self = this;
    if (undefined === ressourceName) ressourceName = 'notes';
    const _URL = url + ressourceName;
    var _isTotallyReceived = false;
    this.isfinished = function() {

    }
    this.lastReceivedObject = undefined;
    this.laststatus = undefined;
    this.lastErrorCode; // soit undefined var non affecté;

    /**
     * 
     * Call function for generic XHR
     * 
     * @param {} method 
     * @param {*} ressource 
     */

    // method, ressourceURL, obj, callback 
    function _callxhr(params) { //(method, ressourceUrl, obj) { // ADRESS serveur + nom de la ressource
        console.log('vous venir de demander un appel XHR');

        var xhr = new XMLHttpRequest();

        xhr.open(params.method, params.ressourceUrl, true);
        xhr.setRequestHeader('Content-type', 'application/json')
        xhr.onreadystatechange = function(params) {
            console.log('readeystate was changed')
            if (this.readyState == this.DONE && (this.status >= 200 && this.status < 300)) {
                //console.log(this.statusText), JSON.parse(this.response);
                if (callback !== params.callback) params.callback(this.response);
            }
        }
        xhr.send((undefined != params.obj) ? JSON.stringify(params.obj) : undefined);
    }

    this.create = function() {
        console.log('read funcion called');
        _callxhr({ method: 'POST', ressourceUrl: _URL });
    }

    this.read = function(id) {
        _isTotallyReceived = false;
        console.log('read funcion called');
        _callxhr({
            method: 'GET',
            ressourceURL: (undefined !== id) ? _URL + '/' + id : _URL,
            callback: function(xhrresponse) {
                _self.lastReceivedObject = parseJSONnote({ _jsonStr: xhrresponse });
                _isTotallyReceived = true;
                console.log('la requete est fini', _self.lastReceivedObject);
            }
        });

        this.update = function(id) {
            console.log('read funcion called');
            _callxhr({ method: 'PUT', ressourceUrl: (_URL + obj.id) });
        }

        this.delete = function(id) {
            if (undefined === id) return;
            console.log('read funcion called');
            _callxhr({ method: 'DELETE', ressourceUrl: (_URL + id) });
        }
    }
}
'use strict;'

//parseJSONnote({ obj, {} });
//parseJSONnote({ jsonStr, {} });



function parseJSONnote(params) {
    var datas = (undefined != params.obj) ? params.obj : JSON.parse(params._jsonStr);

    var isArray = Array.isArray(datas);
    if (!isArray) {
        var date = new Date(datas.date + ' ' + datas.heure);
        delete datas.heure;
        datas.date = date;
        var Note = Object.assign(new Note(), datas);
        makeNote(Note);
        return Note;
    } else {
        var arrayRet = [];
        datas.forEach(element => {
            arrayRet.push(parseJSONnote(undefined({ obj: element })));
        });
        return arrayRet;
    }
}

function Note(_jsonStr) {
    this.desc = '';
    this.date = new Date();
    this.name = '';
    this.abs = function() {

    }

}